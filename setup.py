# Copyright (C) 2020 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from setuptools import setup, find_packages

setup(
    name="snakemake_wrappers",
    version="0.4.1",
    description="Snakemake wrappers and rules that can be included in snakemake workflows.",
    author="Blaise Li",
    author_email="blaise.li@normalesup.org",
    license="GNU GPLv3",
    packages=find_packages(),
    install_requires=[
        "libhts @ git+https://gitlab.pasteur.fr/bli/libhts.git@36d33ac2c366c0f0702ab50fdcab37261add7b9c",
        "libworkflows @ git+https://gitlab.pasteur.fr/bli/libworkflows.git@b29b854ff1db6c87386007808286207b8af11b9d",
        "libreads @ git+https://gitlab.pasteur.fr/bli/libreads.git@91db379cd379f8f12fccdd3840d4369b7f09d444",
        "pandas",
        "snakemake"],
    package_data={
        "smincludes": ["*.rules"],
        "smwrappers": ["*/wrapper.py"]},
    )
