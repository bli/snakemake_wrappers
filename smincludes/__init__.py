from glob import glob
import os
OPJ = os.path.join
OPA = os.path.abspath
OPB = os.path.basename

__copyright__ = "Copyright (C) 2020 Blaise Li"
__licence__ = "GNU GPLv3"
__all__ = ["rules"]

# The working directory is that of the importing context.
# __path__ gives access to the package path
_local_dir, = __path__
_rule_files = glob(OPJ(_local_dir, "*.rules"))
rules = {}
for rule_file in _rule_files:
    rules[OPB(rule_file)[:-6]] = OPA(rule_file)
