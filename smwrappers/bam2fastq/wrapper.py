# Copyright (C) 2020 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from snakemake.shell import shell
from libworkflows import run_with_modules

load_modules = snakemake.config.get("load_modules", False)
if load_modules:
    modules = ["bedtools/2.25.0"]
else:
    modules = []

shell_commands = """
nice -n 19 ionice -c2 -n7 bedtools bamtofastq  \\
    -i {snakemake.input.bam} \\
    -fq {snakemake.output.fastq} \\
    1> {snakemake.log.log} 2> {snakemake.log.err}
"""

shell(run_with_modules(shell_commands, modules))
