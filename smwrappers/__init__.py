import os

__copyright__ = "Copyright (C) 2020 Blaise Li"
__licence__ = "GNU GPLv3"
__all__ = ["wrappers_dir"]

# The working directory is that of the importing context.
# __path__ gives access to the package path
wrappers_dir = __path__[0]
