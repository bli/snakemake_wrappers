# Copyright (C) 2020 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from snakemake import shell
from libworkflows import run_with_modules

load_modules = snakemake.config.get("load_modules", False)
if load_modules:
    modules = ["bioawk", "UCSC-tools/v373"]
else:
    modules = []

shell_commands = """
bigWigAverageOverBed \\
    -bedOut={snakemake.output.bed} \\
    {snakemake.input.bigwig} \\
    {snakemake.params.genome_binned} \\
    /dev/null \\
    2> {snakemake.log.err}
    bioawk -t '{{print $1,$2,$3,$5}}' {snakemake.output.bed} \\
        2>> {snakemake.log.err} \\
        > {snakemake.output.bedgraph}
"""

shell(run_with_modules(shell_commands, modules))
