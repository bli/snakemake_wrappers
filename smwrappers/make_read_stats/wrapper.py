# Copyright (C) 2020 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from contextlib import redirect_stdout, redirect_stderr
from libreads import make_read_statter

with open(snakemake.log.log, "w") as log_file, open(snakemake.log.err, "w") as err_file:
    with redirect_stdout(log_file), redirect_stderr(err_file):

        max_len = snakemake.config.get("max_len", 76)
        print(f"Making read statistics for reads of size up to {max_len}.")
        print(f"Will try to use {snakemake.threads} processes.")

        fastx2read_stats = make_read_statter(
            max_len=max_len, processes=snakemake.threads)

        read_stats = fastx2read_stats(snakemake.input.fastx_filename)

        for (stat_type, stat_table) in read_stats.items():
            stat_table.to_csv(getattr(snakemake.output, stat_type), sep="\t", header=False)
