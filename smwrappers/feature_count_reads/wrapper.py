# Copyright (C) 2020 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from snakemake.shell import shell
from libworkflows import run_with_modules, SHELL_FUNCTIONS

shell.prefix(SHELL_FUNCTIONS)

load_modules = snakemake.config.get("load_modules", False)
if load_modules:
    modules = ["parallel", "subread/1.6.1"]
else:
    modules = []

if not hasattr(snakemake.params, "min_mapq"):
    snakemake.params.min_mapq = ""
if not hasattr(snakemake.params, "overlap"):
    snakemake.params.overlap = "--fracOverlap 1"
if not hasattr(snakemake.params, "other_opts"):
    snakemake.params.other_opts = ""

shell_commands = """
tmpdir=$(mktemp --tmpdir -d {snakemake.params.tmpdir_prefix})
cmd="niceload --noswap featureCounts {snakemake.params.min_mapq} {snakemake.params.other_opts} \\
    -a {snakemake.params.annot} -o {snakemake.output.counts} \\
    -t {snakemake.wildcards.feature_type} -g {snakemake.params.gene_id_name} \\
    -O -s {snakemake.params.stranded} {snakemake.params.overlap} \\
    -T {snakemake.threads} --tmpDir ${{tmpdir}} \\
    {snakemake.input}"
featureCounts -v 2> {snakemake.log.log}
echo ${{cmd}} 1>> {snakemake.log.log}
eval ${{cmd}} 1>> {snakemake.log.log} 2> {snakemake.log.err} || error_exit "featureCounts failed"
rm -rf ${{tmpdir}}
"""

shell(run_with_modules(shell_commands, modules))
