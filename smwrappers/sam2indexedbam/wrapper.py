# Copyright (C) 2020 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from sys import stderr
from snakemake.shell import shell
from libworkflows import run_with_modules
try:
    import libhts
except ModuleNotFoundError:
    stderr.write(
        """
        sam2indexedbam.sh is provided by package libhts,
        which is not installed for this Python version.
        """)
else:
    del libhts

load_modules = snakemake.config.get("load_modules", False)
if load_modules:
    modules = ["samtools/1.9"]
else:
    modules = []

shell_commands = """
echo "Sorting and indexing {snakemake.input.sam}"
export SAMTOOLS_THREADS={snakemake.threads}
export SAMTOOLS_MEM="{snakemake.resources.mem_mb}M"
nice -n 19 ionice -c2 -n7 sam2indexedbam.sh {snakemake.input.sam} 1> {snakemake.log.log} 2> {snakemake.log.err}
"""

shell(run_with_modules(shell_commands, modules))
