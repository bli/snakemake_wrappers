# Copyright (C) 2020 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from snakemake.shell import shell

cmd = """
cmd="htseq-count \\
        -f bam -s {snakemake.params.stranded} -a 0 \\
        -t {snakemake.wildcards.feature_type} \\
        -i gene_id -m {snakemake.params.mode} \\
        {snakemake.input.sorted_bam} {snakemake.params.annot} \\
        > {snakemake.output.counts}"
echo ${{cmd}} > {snakemake.log.log}
eval ${{cmd}} 1>> {snakemake.log.log} 2> {snakemake.log.err} || error_exit "htseq-count failed"
"""

shell(cmd)
