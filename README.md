# Some snakemake wrappers to process high throughput sequencing data.

This package provides snakemake wrappers that I use in my high throughput
sequencing data snakemake workflows.


## Installing

Get the source using `git clone
git@gitlab.pasteur.fr:bli/snakemake_wrappers.git`, `cd` into it and run
`python3 -m pip install .`

It might also work directly:

    python3 -m pip install git+ssh://git@gitlab.pasteur.fr/bli/snamemake_wrappers.git


Your Python installation should then have a `smwrappers` module available.
This module's data includes the wrappers.

## Including snakemake wrappers in your snakefile


The path to the directory containing the wrappers should be given by the
`wrappers_dir` object contained in the `smwrappers` module, and can be imported
as follows:

```{python}
from smwrappers import wrappers_dir
```


To know which wrappers are available, you can explore the content of this directory.
For instance:

```{python}
>>> import os
>>> from smwrappers import wrappers_dir
>>> for dir in os.listdir(wrappers_dir):
...     if not dir.startswith("_"):
...         print(dir)
... 
compute_TPM
map_on_genome
intersect_count_reads
filter_unique_sam
make_read_stats
filter_unique_bam
sam2indexedbam
feature_count_reads
filter_size_bam
compute_RPK
bigwig_average_over_bed
bam2fastq
htseq_count_reads
```


Now you can specify the path to a specific wrapper's directory in the `wrapper`
directive of a snakemake rule. For instance:

```{python}
    wrapper:
        f"file://{wrappers_dir}/map_on_genome"
```

To know precisely how to interface a given wrapper with your rule, you need to
look at the code contained in the `wrapper.py` file contained in the wrapper's
directory. The `snakemake` object represents the rule, and has attributes such
as `input`, `output`, `params`, `threads`, and may sometimes also access to
information defined in the `config` dictionary via `snakemake.config`.


## Citing

If you use this package, please cite the following paper:

> Barucci et al, 2020 (doi: [10.1038/s41556-020-0462-7](https://doi.org/10.1038/s41556-020-0462-7))
